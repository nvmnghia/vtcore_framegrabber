import xml.etree.ElementTree as ET
from PIL import Image
import os

from source import utils


def grab(path_to_xml, path_to_images_folder):
    root = ET.parse(path_to_xml).getroot()
    img_nodes = root.findall('image')

    person_dict = {}

    frame_counter = 0
    for img_node in img_nodes:
        path_to_image = os.path.join(path_to_images_folder, img_node.get('name'))
        image = Image.open(path_to_image)

        # original images are stored in frame folder
        # output images are stored in human folder, which is in the same level as frames
        path_to_output_folder = os.path.join(utils.get_parent_folder(utils.get_parent_folder(path_to_image)),
                                             'person')
        if not os.path.exists(path_to_output_folder):
            os.makedirs(path_to_output_folder)

        for box in img_node:
            identifier = (0 if box.get('label') == 'customer' else 1, int(box[0].text))
            if identifier not in person_dict:
                person_dict[identifier] = len(person_dict)
            person_num = person_dict[identifier]

            path_to_output_image = os.path.join(path_to_output_folder,
                                                ('%05d' % person_num) + '_' + ('%05d' % frame_counter) + '.jpg')

            cropped = image.crop((
                int(float(box.get('xtl'))),
                int(float(box.get('ytl'))),
                int(float(box.get('xbr'))),
                int(float(box.get('ybr')))
            ))
            cropped.save(path_to_output_image)
        frame_counter += 1




