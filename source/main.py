from source import frame_grabber
from source import downloader
from source import utils

from urllib.parse import urlparse
import os


def get_path_to_images(url_to_images):
    parsed_uri = urlparse(url_to_images)
    return '{uri.netloc}'.format(uri=parsed_uri)

if __name__ == '__main__':
    # Set stuffs here
    path_to_xml = '../21_2019-06-10_NVL7_18-59.xml'
    url_to_images = 'http://192.168.25.93:8000/'

    # If the images are already downloaded, comment out this line
    # Downloaded data will be saved to a folder in the same folder with the xml file
    #downloader.download(url_to_images, utils.get_parent_folder(path_to_xml))

    # After downloaded with wget, the images folder's name will be the host name,
    # which default to 192.168.25.93:8000
    # Specify this value otherwise
    current_working_directory = os.path.dirname(os.path.abspath(__file__))
    path_to_images_folder = os.path.join(utils.get_parent_folder(current_working_directory),
                                         get_path_to_images(url_to_images))

    frame_grabber.grab(path_to_xml, path_to_images_folder)


