import subprocess
import os


def download(src, dst):
    """
    Download image folder from src url to dst
    """

    if not os.path.exists(dst):
        os.makedirs(dst)

    subprocess.check_call(['wget',
                           '--recursive',
                           '--no-parent',         # do not go backward to parent folder
                           '-R "index.html*"',    # ignore index.html
                           src],
                          cwd=dst)
