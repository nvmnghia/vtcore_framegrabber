import os


def get_parent_folder(path):
    return os.path.abspath(os.path.join(path, os.pardir))